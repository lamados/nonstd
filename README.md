# nonstd

Various types, functions and methods which should never be used or added to the Go standard library.
