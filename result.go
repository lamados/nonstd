package nonstd

type Result[Value any, Error error] struct {
	value Value
	err   Error
	ok    bool
}

func Ok[Value any, Error error](value Value) Result[Value, Error] {
	return Result[Value, Error]{value: value, ok: true}
}

func Err[Value any, Error error](err Error) Result[Value, Error] {
	return Result[Value, Error]{err: err}
}

func (res Result[Value, Error]) IsOk() bool {
	return res.ok
}

func (res Result[Value, Error]) Ok() Option[Value] {
	return Option[Value]{value: res.value, ok: res.ok}
}

func (res Result[Value, Error]) IsErr() bool {
	return !res.ok
}

func (res Result[Value, Error]) Err() Option[Error] {
	return Option[Error]{value: res.err, ok: !res.ok}
}

func (res Result[Value, Error]) Unwrap() Value {
	if !res.ok {
		panic("no value")
	}

	return res.value
}

func (res Result[Value, Error]) Expect(msg string) Value {
	if !res.ok {
		panic(msg)
	}

	return res.value
}

func (res Result[Value, Error]) UnwrapOr(value Value) Value {
	if !res.ok {
		return value
	}

	return res.value
}

func (res Result[Value, Error]) UnwrapOrElse(fn func() Value) Value {
	if !res.ok {
		return fn()
	}

	return res.value
}

func (res Result[Value, Error]) UnwrapOrDefault() (value Value) {
	if !res.ok {
		return value
	}

	return res.value
}
