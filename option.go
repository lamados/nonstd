package nonstd

type Option[Value any] struct {
	value Value
	ok    bool
}

func Some[Value any](value Value) Option[Value] {
	return Option[Value]{value, true}
}

func None[Value any]() Option[Value] {
	return Option[Value]{}
}

func (opt Option[Value]) IsSome() bool {
	return opt.ok
}

func (opt Option[Value]) IsNone() bool {
	return !opt.ok
}

func (opt Option[Value]) Unwrap() Value {
	if !opt.ok {
		panic("no value")
	}

	return opt.value
}

func (opt Option[Value]) Expect(msg string) Value {
	if !opt.ok {
		panic(msg)
	}

	return opt.value
}

func (opt Option[Value]) UnwrapOr(value Value) Value {
	if !opt.ok {
		return value
	}

	return opt.value
}

func (opt Option[Value]) UnwrapOrElse(fn func() Value) Value {
	if !opt.ok {
		return fn()
	}

	return opt.value
}

func (opt Option[Value]) UnwrapOrDefault() (value Value) {
	if !opt.ok {
		return value
	}

	return opt.value
}

/* UNCOMMENT THE FOLLOWING AFTER GENERIC METHODS ARE RELEASED.

func (opt Option[Value]) OkOr[Error any](err Error) Result[Value, Error] {
	if !opt.ok {
		return Err[Value, Error](err)
	}

	return Ok(opt.value)
}

func (opt Option[Value]) OkOrElse[Error any](fn func() Error) Result[Value, Error] {
	if !opt.ok {
		return Err[Value, Error](fn())
	}

	return Ok(opt.value)
}

*/
